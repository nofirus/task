﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Task.Models
{
    public class MessageViewModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public string MessageTopic { get; set; }

        [Required]
        public string MessageText { get; set; }

    }
}
