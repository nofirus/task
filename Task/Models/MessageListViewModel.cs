﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task.Models
{
    public class MessageListViewModel
    {
        public List<MessageViewModel> MessageList { get; set; }

        public MessageListViewModel()
        {
            MessageList= new List<MessageViewModel>();
        }
    }
}
