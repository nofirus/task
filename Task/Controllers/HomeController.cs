﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task.Models;

namespace Task.Controllers
{
    public class HomeController : Controller
    {
        static MessageListViewModel messageList= new MessageListViewModel();
        
        public IActionResult Index()
        {
            return View(messageList);
        }

        [HttpGet]
        public IActionResult Message()
        {
            ViewData["Message"] = "Write your message";


            return View();
        }

        [HttpPost]
        public IActionResult Message(MessageViewModel messageView)
        {


            if (ModelState.IsValid)
            {
                messageList.MessageList.Add(messageView);
                return Redirect("/Home/Index");

            }

            return View(messageView);

        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
